# sshd configuration role

## Variables
- `sshd_listen_port`: [Integer] Port that sshd will listen on. Defaults to `22`.
- `sshd_listen_addresses`: [List] The IP addresses that sshd listens on. If not defined then there are two possible defaults - if the variable `management_network` is defined, then the role will try to find an interface on the host from `ansible_facts` on that network, and will set sshd to listen on the first one it finds. If `management_network` is undefined, or the role is unable to find an interface on said network, then sshd will listen on `0.0.0.0` (all addresses/interfaces).
- `sshd_login_grace_time`: [String] The server disconnects after this time if the user has not successfully logged in. Defaults to `'2m'`.
- `sshd_allow_root_login`: [Boolean] Whether root is allowed to login. Defaults to `false`.
- `sshd_pubkey_auth`: [Boolean] Whether or not pubkey authentication is enabled. Defaults to `true`.
- `sshd_password_auth`: [Boolean] Whether or not password authentication is enabled. Defaults to `false`.
- `sshd_permit_empty_passwords`: [Boolean] Whether or not empty passwords are allowed. Defaults to `false`.

## Other variables
- `sshd_systemd`: [Optional] Used to control when the systemd service should come up. Useful if sshd is only listening on a VPN and has to wait for the VPN interface to come up. It consists of the following:
  - `after`: [List] Lists units that should be started before the sshd unit.
  - `requires: [List] Lists units on which the sshd unit depends.
